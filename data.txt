<?xml version="1.0" encoding="UTF-8"?>
<GoodreadsResponse>
  <Request>
    <authentication>true</authentication>
      <key><![CDATA[WdmzgesQYrJoq9KPIOQUWg]]></key>
    <method><![CDATA[book_isbn]]></method>
  </Request>
  <book>
  <id>18133</id>
  <title>Lolita</title>
  <isbn><![CDATA[0679723161]]></isbn>
  <isbn13><![CDATA[9780679723165]]></isbn13>
  <asin><![CDATA[]]></asin>
  <kindle_asin><![CDATA[B0089NVIGK]]></kindle_asin>
  <marketplace_id><![CDATA[A21TJRUUN4KGV]]></marketplace_id>
  <country_code><![CDATA[IN]]></country_code>
  <image_url>https://images.gr-assets.com/books/1372767118m/18133.jpg</image_url>
  <small_image_url>https://images.gr-assets.com/books/1372767118s/18133.jpg</small_image_url>
  <publication_year>1989</publication_year>
  <publication_month>3</publication_month>
  <publication_day>13</publication_day>
  <publisher>Vintage International</publisher>
  <language_code>eng</language_code>
  <is_ebook>false</is_ebook>
  <description><![CDATA[Awe and exhiliration--along with heartbreak and mordant wit--abound in Lolita, Nabokov's most famous and controversial novel, which tells the story of the aging Humbert Humbert's obsessive, devouring, and doomed passion for the nymphet Dolores Haze. Lolita is also the story of a hypercivilized European colliding with the cheerful barbarism of postwar America. Most of all, it is a meditation on love--love as outrage and hallucination, madness and transformation.]]></description>
  <work>
  <id type="integer">1268631</id>
  <books_count type="integer">454</books_count>
  <best_book_id type="integer">7604</best_book_id>
  <reviews_count type="integer">1053178</reviews_count>
  <ratings_sum type="integer">2210056</ratings_sum>
  <ratings_count type="integer">568690</ratings_count>
  <text_reviews_count type="integer">20630</text_reviews_count>
  <original_publication_year type="integer">1955</original_publication_year>
  <original_publication_month type="integer" nil="true"/>
  <original_publication_day type="integer" nil="true"/>
  <original_title>Lolita</original_title>
  <original_language_id type="integer" nil="true"/>
  <media_type>book</media_type>
  <rating_dist>5:203535|4:183010|3:118835|2:40526|1:22784|total:568690</rating_dist>
  <desc_user_id type="integer">1290460</desc_user_id>
  <default_chaptering_book_id type="integer">6048235</default_chaptering_book_id>
  <default_description_language_code nil="true"/>
</work>
  <average_rating>3.89</average_rating>
  <num_pages><![CDATA[317]]></num_pages>
  <format><![CDATA[Paperback]]></format>
  <edition_information><![CDATA[50th Anniversary Edition]]></edition_information>
  <ratings_count><![CDATA[17587]]></ratings_count>
  <text_reviews_count><![CDATA[2002]]></text_reviews_count>
  <url><![CDATA[https://www.goodreads.com/book/show/18133.Lolita]]></url>
  <link><![CDATA[https://www.goodreads.com/book/show/18133.Lolita]]></link>
  <authors>
<author>
<id>5152</id>
<name>Vladimir Nabokov</name>
<role></role>
<image_url nophoto='false'>
<![CDATA[https://images.gr-assets.com/authors/1482502806p5/5152.jpg]]>
</image_url>
<small_image_url nophoto='false'>
<![CDATA[https://images.gr-assets.com/authors/1482502806p2/5152.jpg]]>
</small_image_url>
<link><![CDATA[https://www.goodreads.com/author/show/5152.Vladimir_Nabokov]]></link>
<average_rating>3.88</average_rating>
<ratings_count>993805</ratings_count>
<text_reviews_count>37854</text_reviews_count>
</author>
<author>
<id>7820029</id>
<name>John Ray Jr.</name>
<role>Preface</role>
<image_url nophoto='true'>
<![CDATA[https://s.gr-assets.com/assets/nophoto/user/u_200x266-e183445fd1a1b5cc7075bb1cf7043306.png]]>
</image_url>
<small_image_url nophoto='true'>
<![CDATA[https://s.gr-assets.com/assets/nophoto/user/u_50x66-632230dc9882b4352d753eedf9396530.png]]>
</small_image_url>
<link><![CDATA[https://www.goodreads.com/author/show/7820029.John_Ray_Jr_]]></link>
<average_rating>0.0</average_rating>
<ratings_count>0</ratings_count>
<text_reviews_count>0</text_reviews_count>
</author>
</authors>

    <reviews_widget>
      <![CDATA[
        <style>
  #goodreads-widget {
    font-family: georgia, serif;
    padding: 18px 0;
    width:565px;
  }
  #goodreads-widget h1 {
    font-weight:normal;
    font-size: 16px;
    border-bottom: 1px solid #BBB596;
    margin-bottom: 0;
  }
  #goodreads-widget a {
    text-decoration: none;
    color:#660;
  }
  iframe{
    background-color: #fff;
  }
  #goodreads-widget a:hover { text-decoration: underline; }
  #goodreads-widget a:active {
    color:#660;
  }
  #gr_footer {
    width: 100%;
    border-top: 1px solid #BBB596;
    text-align: right;
  }
  #goodreads-widget .gr_branding{
    color: #382110;
    font-size: 11px;
    text-decoration: none;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
</style>
<div id="goodreads-widget">
  <div id="gr_header"><h1><a rel="nofollow" href="https://www.goodreads.com/book/show/18133.Lolita">Lolita Reviews</a></h1></div>
  <iframe id="the_iframe" src="https://www.goodreads.com/api/reviews_widget_iframe?did=DEVELOPER_ID&amp;format=html&amp;isbn=0679723161&amp;links=660&amp;min_rating=&amp;review_back=fff&amp;stars=000&amp;text=000" width="565" height="400" frameborder="0"></iframe>
  <div id="gr_footer">
    <a class="gr_branding" target="_blank" rel="nofollow" href="https://www.goodreads.com/book/show/18133.Lolita?utm_medium=api&amp;utm_source=reviews_widget">Reviews from Goodreads.com</a>
  </div>
</div>

      ]]>
    </reviews_widget>
  <popular_shelves>
      <shelf name="to-read" count="433848"/>
      <shelf name="currently-reading" count="21011"/>
      <shelf name="classics" count="10595"/>
      <shelf name="favorites" count="7817"/>
      <shelf name="fiction" count="7249"/>
      <shelf name="classic" count="1650"/>
      <shelf name="owned" count="1504"/>
      <shelf name="books-i-own" count="1303"/>
      <shelf name="literature" count="1287"/>
      <shelf name="favourites" count="907"/>
      <shelf name="novels" count="800"/>
      <shelf name="russian" count="784"/>
      <shelf name="to-buy" count="604"/>
      <shelf name="romance" count="548"/>
      <shelf name="book-club" count="503"/>
      <shelf name="russian-literature" count="491"/>
      <shelf name="owned-books" count="466"/>
      <shelf name="1001-books" count="446"/>
      <shelf name="literary-fiction" count="415"/>
      <shelf name="adult" count="397"/>
      <shelf name="20th-century" count="388"/>
      <shelf name="novel" count="386"/>
      <shelf name="russia" count="352"/>
      <shelf name="library" count="348"/>
      <shelf name="abandoned" count="332"/>
      <shelf name="modern-classics" count="319"/>
      <shelf name="classics-to-read" count="296"/>
      <shelf name="american" count="272"/>
      <shelf name="1001" count="270"/>
      <shelf name="unfinished" count="268"/>
      <shelf name="audiobooks" count="265"/>
      <shelf name="russian-lit" count="260"/>
      <shelf name="audiobook" count="259"/>
      <shelf name="dnf" count="251"/>
      <shelf name="classic-literature" count="245"/>
      <shelf name="adult-fiction" count="244"/>
      <shelf name="banned-books" count="237"/>
      <shelf name="did-not-finish" count="222"/>
      <shelf name="default" count="220"/>
      <shelf name="my-library" count="220"/>
      <shelf name="kindle" count="218"/>
      <shelf name="general-fiction" count="196"/>
      <shelf name="english" count="195"/>
      <shelf name="wish-list" count="195"/>
      <shelf name="all-time-favorites" count="191"/>
      <shelf name="to-read-classics" count="186"/>
      <shelf name="on-hold" count="182"/>
      <shelf name="literary" count="177"/>
      <shelf name="my-books" count="177"/>
      <shelf name="1001-books-to-read-before-you-die" count="177"/>
      <shelf name="re-read" count="177"/>
      <shelf name="american-literature" count="169"/>
      <shelf name="drama" count="163"/>
      <shelf name="ebook" count="163"/>
      <shelf name="favorite-books" count="161"/>
      <shelf name="contemporary" count="160"/>
      <shelf name="to-re-read" count="150"/>
      <shelf name="i-own" count="149"/>
      <shelf name="bookclub" count="146"/>
      <shelf name="classic-fiction" count="142"/>
      <shelf name="audio" count="142"/>
      <shelf name="classic-lit" count="139"/>
      <shelf name="tbr" count="133"/>
      <shelf name="must-read" count="132"/>
      <shelf name="lit" count="132"/>
      <shelf name="nabokov" count="131"/>
      <shelf name="1950s" count="123"/>
      <shelf name="to-reread" count="121"/>
      <shelf name="favorite" count="120"/>
      <shelf name="school" count="120"/>
      <shelf name="to-read-fiction" count="118"/>
      <shelf name="read-in-2015" count="115"/>
      <shelf name="books" count="113"/>
      <shelf name="psychology" count="112"/>
      <shelf name="usa" count="105"/>
      <shelf name="read-in-2016" count="104"/>
      <shelf name="erotica" count="103"/>
      <shelf name="didn-t-finish" count="102"/>
      <shelf name="banned" count="98"/>
      <shelf name="love" count="98"/>
      <shelf name="ebooks" count="96"/>
      <shelf name="reread" count="96"/>
      <shelf name="bookshelf" count="94"/>
      <shelf name="disturbing" count="93"/>
      <shelf name="controversial" count="93"/>
      <shelf name="never-finished" count="91"/>
      <shelf name="read-in-2013" count="91"/>
      <shelf name="audible" count="90"/>
      <shelf name="own-it" count="89"/>
      <shelf name="on-my-shelf" count="88"/>
      <shelf name="faves" count="88"/>
      <shelf name="college" count="86"/>
      <shelf name="gave-up-on" count="85"/>
      <shelf name="personal-library" count="81"/>
      <shelf name="read-in-2018" count="80"/>
      <shelf name="sexuality" count="80"/>
      <shelf name="dark" count="80"/>
      <shelf name="modern-library-100" count="80"/>
      <shelf name="vladimir-nabokov" count="79"/>
      <shelf name="favourite" count="77"/>
  </popular_shelves>
  <book_links>
    <book_link>
  <id>8</id>
  <name>Libraries</name>
  <link>https://www.goodreads.com/book_link/follow/8</link>
</book_link>

  </book_links>
  <buy_links>
    <buy_link>
<id>1</id>
<name>Amazon</name>
<link>https://www.goodreads.com/book_link/follow/1</link>
</buy_link>
<buy_link>
<id>10</id>
<name>Audible</name>
<link>https://www.goodreads.com/book_link/follow/10</link>
</buy_link>
<buy_link>
<id>3</id>
<name>Barnes &amp; Noble</name>
<link>https://www.goodreads.com/book_link/follow/3</link>
</buy_link>
<buy_link>
<id>1027</id>
<name>Walmart eBooks</name>
<link>https://www.goodreads.com/book_link/follow/1027</link>
</buy_link>
<buy_link>
<id>2102</id>
<name>Apple Books</name>
<link>https://www.goodreads.com/book_link/follow/2102</link>
</buy_link>
<buy_link>
<id>8036</id>
<name>Google Play</name>
<link>https://www.goodreads.com/book_link/follow/8036</link>
</buy_link>
<buy_link>
<id>4</id>
<name>Abebooks</name>
<link>https://www.goodreads.com/book_link/follow/4</link>
</buy_link>
<buy_link>
<id>882</id>
<name>Book Depository</name>
<link>https://www.goodreads.com/book_link/follow/882</link>
</buy_link>
<buy_link>
<id>9</id>
<name>Indigo</name>
<link>https://www.goodreads.com/book_link/follow/9</link>
</buy_link>
<buy_link>
<id>5</id>
<name>Alibris</name>
<link>https://www.goodreads.com/book_link/follow/5</link>
</buy_link>
<buy_link>
<id>107</id>
<name>Better World Books</name>
<link>https://www.goodreads.com/book_link/follow/107</link>
</buy_link>
<buy_link>
<id>7</id>
<name>IndieBound</name>
<link>https://www.goodreads.com/book_link/follow/7</link>
</buy_link>

  </buy_links>
  <series_works>
    
  </series_works>
  <similar_books>
          <book>
<id>45791</id>
<title><![CDATA[The Ballad of the Sad Café and Other Stories]]></title>
<title_without_series><![CDATA[The Ballad of the Sad Café and Other Stories]]></title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/45791.The_Ballad_of_the_Sad_Caf_and_Other_Stories]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>152</num_pages>
<work>
<id>952665</id>
</work>
<isbn>0618565868</isbn>
<isbn13>9780618565863</isbn13>
<average_rating>4.02</average_rating>
<ratings_count>9486</ratings_count>
<publication_year>2005</publication_year>
<publication_month>4</publication_month>
<publication_day>5</publication_day>
<authors>
<author>
<id>3506</id>
<name>Carson McCullers</name>
<link><![CDATA[https://www.goodreads.com/author/show/3506.Carson_McCullers]]></link>
</author>
</authors>
</book>

          <book>
<id>46114</id>
<title>The Waves</title>
<title_without_series>The Waves</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/46114.The_Waves]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>297</num_pages>
<work>
<id>6057263</id>
</work>
<isbn>0156949601</isbn>
<isbn13>9780156949606</isbn13>
<average_rating>4.14</average_rating>
<ratings_count>22441</ratings_count>
<publication_year>1978</publication_year>
<publication_month>6</publication_month>
<publication_day>1</publication_day>
<authors>
<author>
<id>6765</id>
<name>Virginia Woolf</name>
<link><![CDATA[https://www.goodreads.com/author/show/6765.Virginia_Woolf]]></link>
</author>
</authors>
</book>

          <book>
<id>31072</id>
<title>Under the Volcano</title>
<title_without_series>Under the Volcano</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/31072.Under_the_Volcano]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>397</num_pages>
<work>
<id>1321805</id>
</work>
<isbn>0060955228</isbn>
<isbn13>9780060955229</isbn13>
<average_rating>3.80</average_rating>
<ratings_count>19655</ratings_count>
<publication_year>2000</publication_year>
<publication_month>4</publication_month>
<publication_day>26</publication_day>
<authors>
<author>
<id>17439</id>
<name>Malcolm Lowry</name>
<link><![CDATA[https://www.goodreads.com/author/show/17439.Malcolm_Lowry]]></link>
</author>
</authors>
</book>

          <book>
<id>48328</id>
<title>Revolutionary Road</title>
<title_without_series>Revolutionary Road</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/48328.Revolutionary_Road]]></link>
<small_image_url><![CDATA[https://images.gr-assets.com/books/1353721885s/48328.jpg]]></small_image_url>
<image_url><![CDATA[https://images.gr-assets.com/books/1353721885m/48328.jpg]]></image_url>
<num_pages>355</num_pages>
<work>
<id>1235136</id>
</work>
<isbn>0413757102</isbn>
<isbn13>9780413757104</isbn13>
<average_rating>3.90</average_rating>
<ratings_count>70455</ratings_count>
<publication_year>2001</publication_year>
<publication_month>2</publication_month>
<publication_day>1</publication_day>
<authors>
<author>
<id>27069</id>
<name>Richard Yates</name>
<link><![CDATA[https://www.goodreads.com/author/show/27069.Richard_Yates]]></link>
</author>
</authors>
</book>

          <book>
<id>374233</id>
<title><![CDATA[If on a Winter's Night a Traveler]]></title>
<title_without_series><![CDATA[If on a Winter's Night a Traveler]]></title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/374233.If_on_a_Winter_s_Night_a_Traveler]]></link>
<small_image_url><![CDATA[https://images.gr-assets.com/books/1528312857s/374233.jpg]]></small_image_url>
<image_url><![CDATA[https://images.gr-assets.com/books/1528312857m/374233.jpg]]></image_url>
<num_pages>260</num_pages>
<work>
<id>1116802</id>
</work>
<isbn></isbn>
<isbn13>9780088619451</isbn13>
<average_rating>4.07</average_rating>
<ratings_count>62021</ratings_count>
<publication_year>1982</publication_year>
<publication_month>10</publication_month>
<publication_day>20</publication_day>
<authors>
<author>
<id>155517</id>
<name>Italo Calvino</name>
<link><![CDATA[https://www.goodreads.com/author/show/155517.Italo_Calvino]]></link>
</author>
</authors>
</book>

          <book>
<id>115476</id>
<title>Nostromo</title>
<title_without_series>Nostromo</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/115476.Nostromo]]></link>
<small_image_url><![CDATA[https://images.gr-assets.com/books/1328865264s/115476.jpg]]></small_image_url>
<image_url><![CDATA[https://images.gr-assets.com/books/1328865264m/115476.jpg]]></image_url>
<num_pages>336</num_pages>
<work>
<id>678519</id>
</work>
<isbn>0486424529</isbn>
<isbn13>9780486424521</isbn13>
<average_rating>3.81</average_rating>
<ratings_count>13940</ratings_count>
<publication_year>2002</publication_year>
<publication_month>12</publication_month>
<publication_day>31</publication_day>
<authors>
<author>
<id>3345</id>
<name>Joseph Conrad</name>
<link><![CDATA[https://www.goodreads.com/author/show/3345.Joseph_Conrad]]></link>
</author>
</authors>
</book>

          <book>
<id>10775</id>
<title>The Garden of Eden</title>
<title_without_series>The Garden of Eden</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/10775.The_Garden_of_Eden]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>248</num_pages>
<work>
<id>476408</id>
</work>
<isbn>0684804522</isbn>
<isbn13>9780684804521</isbn13>
<average_rating>3.74</average_rating>
<ratings_count>15201</ratings_count>
<publication_year>2003</publication_year>
<publication_month></publication_month>
<publication_day></publication_day>
<authors>
<author>
<id>1455</id>
<name>Ernest Hemingway</name>
<link><![CDATA[https://www.goodreads.com/author/show/1455.Ernest_Hemingway]]></link>
</author>
</authors>
</book>

          <book>
<id>126603</id>
<title>Studs Lonigan</title>
<title_without_series>Studs Lonigan</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/126603.Studs_Lonigan]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>988</num_pages>
<work>
<id>121922</id>
</work>
<isbn>1931082553</isbn>
<isbn13>9781931082556</isbn13>
<average_rating>3.80</average_rating>
<ratings_count>2077</ratings_count>
<publication_year>2004</publication_year>
<publication_month></publication_month>
<publication_day></publication_day>
<authors>
<author>
<id>6165</id>
<name>James T. Farrell</name>
<link><![CDATA[https://www.goodreads.com/author/show/6165.James_T_Farrell]]></link>
</author>
</authors>
</book>

          <book>
<id>261441</id>
<title><![CDATA[USA: The 42nd Parallel / 1919 / The Big Money]]></title>
<title_without_series><![CDATA[USA: The 42nd Parallel / 1919 / The Big Money]]></title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/261441.USA]]></link>
<small_image_url><![CDATA[https://images.gr-assets.com/books/1309200622s/261441.jpg]]></small_image_url>
<image_url><![CDATA[https://images.gr-assets.com/books/1309200622m/261441.jpg]]></image_url>
<num_pages>1312</num_pages>
<work>
<id>6503267</id>
</work>
<isbn>1883011140</isbn>
<isbn13>9781883011147</isbn13>
<average_rating>4.09</average_rating>
<ratings_count>4353</ratings_count>
<publication_year>1996</publication_year>
<publication_month></publication_month>
<publication_day></publication_day>
<authors>
<author>
<id>4778</id>
<name>John Dos Passos</name>
<link><![CDATA[https://www.goodreads.com/author/show/4778.John_Dos_Passos]]></link>
</author>
</authors>
</book>

          <book>
<id>32071</id>
<title>Sons and Lovers</title>
<title_without_series>Sons and Lovers</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/32071.Sons_and_Lovers]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>654</num_pages>
<work>
<id>3173046</id>
</work>
<isbn>0375753737</isbn>
<isbn13>9780375753732</isbn13>
<average_rating>3.62</average_rating>
<ratings_count>41957</ratings_count>
<publication_year>1999</publication_year>
<publication_month>8</publication_month>
<publication_day>17</publication_day>
<authors>
<author>
<id>17623</id>
<name>D.H. Lawrence</name>
<link><![CDATA[https://www.goodreads.com/author/show/17623.D_H_Lawrence]]></link>
</author>
</authors>
</book>

          <book>
<id>40470</id>
<title><![CDATA[Cities of the Plain (The Border Trilogy, #3)]]></title>
<title_without_series>Cities of the Plain</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/40470.Cities_of_the_Plain]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>292</num_pages>
<work>
<id>3049974</id>
</work>
<isbn>0679747192</isbn>
<isbn13>9780679747192</isbn13>
<average_rating>4.08</average_rating>
<ratings_count>17526</ratings_count>
<publication_year>1999</publication_year>
<publication_month>5</publication_month>
<publication_day>25</publication_day>
<authors>
<author>
<id>4178</id>
<name>Cormac McCarthy</name>
<link><![CDATA[https://www.goodreads.com/author/show/4178.Cormac_McCarthy]]></link>
</author>
</authors>
</book>

          <book>
<id>616052</id>
<title>On the Eve</title>
<title_without_series>On the Eve</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/616052.On_the_Eve]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>180</num_pages>
<work>
<id>2578778</id>
</work>
<isbn>1426450435</isbn>
<isbn13>9781426450433</isbn13>
<average_rating>3.82</average_rating>
<ratings_count>2511</ratings_count>
<publication_year>2008</publication_year>
<publication_month>5</publication_month>
<publication_day>29</publication_day>
<authors>
<author>
<id>410680</id>
<name>Ivan Turgenev</name>
<link><![CDATA[https://www.goodreads.com/author/show/410680.Ivan_Turgenev]]></link>
</author>
</authors>
</book>

          <book>
<id>53130</id>
<title><![CDATA[Justine, Philosophy in the Bedroom, and Other Writings]]></title>
<title_without_series><![CDATA[Justine, Philosophy in the Bedroom, and Other Writings]]></title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/53130.Justine_Philosophy_in_the_Bedroom_and_Other_Writings]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>784</num_pages>
<work>
<id>208955</id>
</work>
<isbn>0802132189</isbn>
<isbn13>9780802132185</isbn13>
<average_rating>3.73</average_rating>
<ratings_count>3055</ratings_count>
<publication_year>1994</publication_year>
<publication_month>1</publication_month>
<publication_day>11</publication_day>
<authors>
<author>
<id>2885224</id>
<name>Marquis de Sade</name>
<link><![CDATA[https://www.goodreads.com/author/show/2885224.Marquis_de_Sade]]></link>
</author>
</authors>
</book>

          <book>
<id>373755</id>
<title>Absalom, Absalom!</title>
<title_without_series>Absalom, Absalom!</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/373755.Absalom_Absalom_]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>320</num_pages>
<work>
<id>1595511</id>
</work>
<isbn>0679732187</isbn>
<isbn13>9780679732181</isbn13>
<average_rating>3.96</average_rating>
<ratings_count>34907</ratings_count>
<publication_year>1991</publication_year>
<publication_month>1</publication_month>
<publication_day>30</publication_day>
<authors>
<author>
<id>3535</id>
<name>William Faulkner</name>
<link><![CDATA[https://www.goodreads.com/author/show/3535.William_Faulkner]]></link>
</author>
</authors>
</book>

          <book>
<id>17881</id>
<title><![CDATA[Notes from Underground & The Double]]></title>
<title_without_series><![CDATA[Notes from Underground & The Double]]></title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/17881.Notes_from_Underground_The_Double]]></link>
<small_image_url><![CDATA[https://images.gr-assets.com/books/1399385623s/17881.jpg]]></small_image_url>
<image_url><![CDATA[https://images.gr-assets.com/books/1399385623m/17881.jpg]]></image_url>
<num_pages>287</num_pages>
<work>
<id>48319492</id>
</work>
<isbn>0140442529</isbn>
<isbn13>9780140442526</isbn13>
<average_rating>4.19</average_rating>
<ratings_count>4937</ratings_count>
<publication_year>1972</publication_year>
<publication_month>7</publication_month>
<publication_day>30</publication_day>
<authors>
<author>
<id>3137322</id>
<name>Fyodor Dostoyevsky</name>
<link><![CDATA[https://www.goodreads.com/author/show/3137322.Fyodor_Dostoyevsky]]></link>
</author>
</authors>
</book>

          <book>
<id>70561</id>
<title><![CDATA[The Gulag Archipelago 1918-1956]]></title>
<title_without_series><![CDATA[The Gulag Archipelago 1918-1956]]></title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/70561.The_Gulag_Archipelago_1918_1956]]></link>
<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>
<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>
<num_pages>472</num_pages>
<work>
<id>2944012</id>
</work>
<isbn>0060007761</isbn>
<isbn13>9780060007768</isbn13>
<average_rating>4.20</average_rating>
<ratings_count>15095</ratings_count>
<publication_year>2002</publication_year>
<publication_month>2</publication_month>
<publication_day>1</publication_day>
<authors>
<author>
<id>10420</id>
<name>Aleksandr Solzhenitsyn</name>
<link><![CDATA[https://www.goodreads.com/author/show/10420.Aleksandr_Solzhenitsyn]]></link>
</author>
</authors>
</book>

          <book>
<id>113205</id>
<title>Heart of a Dog</title>
<title_without_series>Heart of a Dog</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/113205.Heart_of_a_Dog]]></link>
<small_image_url><![CDATA[https://images.gr-assets.com/books/1457906509s/113205.jpg]]></small_image_url>
<image_url><![CDATA[https://images.gr-assets.com/books/1457906509m/113205.jpg]]></image_url>
<num_pages>126</num_pages>
<work>
<id>2470656</id>
</work>
<isbn>0802150594</isbn>
<isbn13>9780802150592</isbn13>
<average_rating>4.14</average_rating>
<ratings_count>31177</ratings_count>
<publication_year>1994</publication_year>
<publication_month>1</publication_month>
<publication_day>21</publication_day>
<authors>
<author>
<id>3873</id>
<name>Mikhail Bulgakov</name>
<link><![CDATA[https://www.goodreads.com/author/show/3873.Mikhail_Bulgakov]]></link>
</author>
</authors>
</book>

          <book>
<id>338798</id>
<title>Ulysses</title>
<title_without_series>Ulysses</title_without_series>
<link><![CDATA[https://www.goodreads.com/book/show/338798.Ulysses]]></link>
<small_image_url><![CDATA[https://images.gr-assets.com/books/1428891345s/338798.jpg]]></small_image_url>
<image_url><![CDATA[https://images.gr-assets.com/books/1428891345m/338798.jpg]]></image_url>
<num_pages>783</num_pages>
<work>
<id>2368224</id>
</work>
<isbn>0679722769</isbn>
<isbn13>9780679722762</isbn13>
<average_rating>3.74</average_rating>
<ratings_count>96242</ratings_count>
<publication_year>1990</publication_year>
<publication_month></publication_month>
<publication_day></publication_day>
<authors>
<author>
<id>5144</id>
<name>James Joyce</name>
<link><![CDATA[https://www.goodreads.com/author/show/5144.James_Joyce]]></link>
</author>
</authors>
</book>

  </similar_books>
</book>

</GoodreadsResponse>
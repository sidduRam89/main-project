/* eslint-disable no-console */
const mysql = require('promise-mysql');

async function dbConnection() {
  const db = await mysql.createConnection({
    host: 'localhost',
    user: 'siddu',
    password: 'iamsiddu',
    database: process.env.testDB || 'main_project',
  });
  return db;
}

async function getBooksData() {
  let booksData;
  try {
    const db = await dbConnection();
    const query = 'select bookData.name,authorData.name as authorName,bookData.authorId,bookData.description,bookData.ISBN,bookData.paperback,bookData.publisher,bookData.review,bookData.link from bookData right join authorData on authorData.id = bookData.authorId';
    booksData = await db.query(query);
    db.end();
  } catch (err) {
    console.error(err);
  }
  return booksData;
}
async function getBookByIsbn(ISBN) {
  let booksData;
  try {
    const db = await dbConnection();
    const query = 'select bookData.name,authorData.name as authorName,bookData.authorId,bookData.description,bookData.ISBN,bookData.paperback,bookData.publisher,bookData.review,bookData.link from bookData right join authorData on authorData.id = bookData.authorId where ISBN = ?';
    booksData = await db.query(query, [ISBN]);
    db.end();
  } catch (err) {
    console.error(err);
  }
  return booksData;
}
module.exports = {
  getBooksData,
  getBookByIsbn,
};

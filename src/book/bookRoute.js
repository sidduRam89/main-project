/* eslint-disable no-console */
const bookRouter = require('express').Router();
const { getBooksData, getBookByIsbn } = require('./bookContoller.js');
const { redirectLogin } = require('../../middlewares/redirectMiddleware.js');
const { getReviewByIsbn } = require('./goodReadsReview.js');

bookRouter.get('/book', redirectLogin, async (req, res) => {
  try {
    const booksData = await getBooksData();
    res.render('book', { booksData });
  } catch (err) {
    console.error(err);
  }
});
bookRouter.get('/book/:ISBN', redirectLogin, async (req, res, next) => {
  try {
    const ISBN = parseInt(req.params.ISBN, 10);
    const book = await getBookByIsbn(ISBN);
    const review = await getReviewByIsbn(ISBN);
    if (book.length !== 0) {
      res.render('bookDetail', { book: book[0], review });
    } else {
      next();
    }
  } catch (err) {
    console.error(err);
  }
});
module.exports = {
  bookRouter,
};

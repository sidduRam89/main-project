const mongoose = require('mongoose');

function createMongoConnection() {
  mongoose.connect('mongodb://localhost:27017/userRegistered', { useNewUrlParser: true });
}

module.exports = {
  createMongoConnection,
};

const redirectLogin = (req, res, next) => {
  if (req.session.user) {
    next();
  } else {
    res.status(401);
    res.render('unauthorizedUser');
  }
};
const redirectHome = (req, res, next) => {
  if (req.session.user) {
    res.redirect('/index');
  } else {
    next();
  }
};

module.exports = {
  redirectLogin,
  redirectHome,
};

/* eslint-disable no-console */
const authorRouter = require('express').Router();
const { getAuthorsData, getAuthorById } = require('./authorController.js');
const { redirectLogin } = require('../../middlewares/redirectMiddleware.js');

authorRouter.get('/author', redirectLogin, async (req, res) => {
  try {
    const authorsData = await getAuthorsData();
    res.render('author', { authorsData });
  } catch (err) {
    console.error(err);
  }
});
authorRouter.get('/author/:id', redirectLogin, async (req, res, next) => {
  try {
    const authorsData = await getAuthorById(parseInt(req.params.id, 10));
    if (Object.keys(authorsData).length !== 0) {
      res.render('authorDetail', { author: authorsData });
    } else {
      next();
    }
  } catch (err) {
    console.error(err);
  }
});
module.exports = {
  authorRouter,
};

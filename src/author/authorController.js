/* eslint-disable no-console */
const mysql = require('promise-mysql');

function authorDataController(authorsData) {
  const authorData = { ...authorsData[0] };
  if (authorsData.length === 0) {
    return authorData;
  }
  delete authorData.bookName;
  delete authorData.ISBN;
  authorData.book = [];
  authorsData.forEach((data) => {
    authorData.book.push([data.bookName, data.ISBN]);
  });
  return authorData;
}
async function dbConnection() {
  const db = await mysql.createConnection({
    host: 'localhost',
    user: 'siddu',
    password: 'iamsiddu',
    database: process.env.testDB || 'main_project',
  });
  return db;
}
async function getAuthorsData() {
  let authorsData;
  try {
    const db = await dbConnection();
    const query = 'select name,id from authorData';
    authorsData = await db.query(query);
    db.end();
  } catch (err) {
    console.error(err);
  }
  return authorsData;
}
async function getAuthorById(id) {
  let authorsData;
  try {
    const db = await dbConnection();
    const query = 'select authorData.name,authorData.description,authorData.born,authorData.died,authorData.occupation,authorData.id,authorData.link,bookData.name as bookName,bookData.ISBN from authorData right join bookData on authorData.id=bookData.authorId where id = ?';
    authorsData = await db.query(query, [id]);
    db.end();
  } catch (err) {
    console.error(err);
  }
  return (authorDataController(authorsData));
}

module.exports = {
  getAuthorsData,
  getAuthorById,
};

const authorsData = [{
    name: 'Vladimir_Nabokov',
    book: [{
        name: "Lolita",
        ISBN: 9780679723165
      },
      {
        name: 'Pale Fire',
        ISBN: 9780141185262
      },
      {
        name: 'Laughter in the dark',
        ISBN: 0141186526
      }
    ],
    description: "Nabokov was born on 22 April 1899 (10 April 1899 Old Style), in Saint Petersburg,[a] to a wealthy and prominent family of the Russian nobility, which traced its roots back to a fourteenth-century Tatar prince, Nabok Murza, who entered into the service of the Tsars, and from whom the family name is derived.[4][5]:16[6] His father was the liberal lawyer, statesman, and journalist Vladimir Dmitrievich Nabokov (1870–1922) and his mother was the heiress Yelena Ivanovna née Rukavishnikova, the granddaughter of a millionaire gold-mine owner. His father was a leader of the pre-Revolutionary liberal Constitutional Democratic Party and authored numerous books and articles about criminal law and politics.[7] His cousins included the composer Nicolas Nabokov. His paternal grandfather, Dmitry Nabokov (1827–1904), had been Russia's Justice Minister in the reign of Alexander II. His paternal grandmother was the Baltic German Baroness Maria von Korff (1842–1926). Through his father's German ancestry, he was also related to the music composer Carl Heinrich Graun (1704–1759).Vladimir was the family's eldest and favorite child, with four younger siblings: Sergey (1900–45); Olga (1903–78); Elena (1906–2000) and Kiril (1912–64). Sergey was killed in a Nazi concentration camp in 1945, after he spoke out publicly denouncing Hitler's regime. Olga is recalled by Ayn Rand (her close friend at Stoiunina Gymnasium) as having been a supporter of constitutional monarchy who had first awakened Rand's interest in politics.[9][10] The youngest daughter Elena, who would in later years become Vladimir's favourite sibling, published her correspondence with her brother in 1985 and would become an important living source for later biographers of Nabokov.",
    born: "22 April [O.S. 10 April] 1899 Saint Petersburg, Russian Empire",
    died: "2 July 1977 (aged 78) Montreux, Switzerland",
    occupation: "Novelist, professor",
    id: 1,
    link: "https://en.wikipedia.org/wiki/Vladimir_Nabokov"
  },
  {
    name: 'James_Joyce',
    book: [{
      name: 'Ulysees',
      ISBN: 9781604598636
    }, {
      name: 'Dubliners',
      ISBN: 6069831195
    }, {
      name: 'The Dead',
      ISBN: 1629101648
    }, {
      name: "Araby",
      ISBN: 1502734524
    }],
    description: "James Augustine[1] Aloysius Joyce (2 February 1882 – 13 January 1941) was an Irish novelist, short story writer, and poet. He contributed to the modernist avant-garde and is regarded as one of the most influential and important names of the 20th century. Joyce is best known for Ulysses (1922), a landmark work in which the episodes of Homer's Odyssey are paralleled in a variety of literary styles, most famously stream of consciousness. Other well-known works are the short-story collection Dubliners (1914), and the novels A Portrait of the Artist as a Young Man (1916) and Finnegans Wake (1939). His other writings include three books of poetry, a play, his published letters and occasional journalism.Joyce was born in Dublin into a middle-class family. A brilliant student, he briefly attended the Christian Brothers-run O'Connell School before excelling at the Jesuit schools Clongowes and Belvedere, despite the chaotic family life imposed by his father's alcoholism and unpredictable finances. He went on to attend University College Dublin.In 1904, in his early twenties, Joyce emigrated to continental Europe with his partner (and later wife) Nora Barnacle. They lived in Trieste, Paris, and Zurich. Although most of his adult life was spent abroad, Joyce's fictional universe centres on Dublin and is populated largely by characters who closely resemble family members, enemies and friends from his time there. Ulysses in particular is set with precision in the streets and alleyways of the city. Shortly after the publication of Ulysses, he elucidated this preoccupation somewhat, saying,For myself, I always write about Dublin, because if I can get to the heart of Dublin I can get to the heart of all the cities of the world. In the particular is contained the universal",
    born: "James Joyce born on 2 Febraury in Dublin",
    died: "13 January 1941 age 59",
    occupation: "novelist, short story writer, and poet",
    id: 2,
    link: "https://en.wikipedia.org/wiki/James_Joyce"
  },
  {
    name: 'Mark_Twain',
    book: [{
        name: 'The Adventures of Huckleberry Finn',
        ISBN: 0141439645
      },
      {
        name: 'The adventures of Tom sawyer',
        ISBN: 1503215679
      }
    ],
    description: "Mark Twain (November 30, 1835 – April 21, 1910),[1] real name Samuel Langhorne Clemens, was an American writer, humorist, entrepreneur, publisher, and lecturer. Among his novels are The Adventures of Tom Sawyer (1876) and its sequel, the Adventures of Huckleberry Finn (1885),[2] the latter often called The Great American Novel.Twain was raised in Hannibal, Missouri, which later provided the setting for Tom Sawyer and Huckleberry Finn. He served an apprenticeship with a printer and then worked as a typesetter, contributing articles to the newspaper of his older brother Orion Clemens. He later became a riverboat pilot on the Mississippi River before heading west to join Orion in Nevada. He referred humorously to his lack of success at mining, turning to journalism for the Virginia City Territorial Enterprise.[3] His humorous story,The Celebrated Jumping Frog of Calaveras County, was published in 1865, based on a story that he heard at Angels Hotel in Angels Camp, California, where he had spent some time as a miner. The short story brought international attention and was even translated into French.[4] His wit and satire, in prose and in speech, earned praise from critics and peers, and he was a friend to presidents, artists, industrialists, and European royalty.Twain earned a great deal of money from his writings and lectures, but he invested in ventures that lost most of it—notably the Paige Compositor, a mechanical typesetter that failed because of its complexity and imprecision. He filed for bankruptcy in the wake of these financial setbacks, but he eventually overcame his financial troubles with the help of Henry Huttleston Rogers. He chose to pay all his pre-bankruptcy creditors in full, even after he had no legal responsibility to do so.Twain was born shortly after an appearance of Halley's Comet, and he predicted that he would go out with it as well; he died the day after the comet returned. He was lauded as the greatest humorist this country has produced,[5] and William Faulkner called him the father of American literature",
    born: "Samuel Langhorne Clemen November 30, 1835 Florida, Missouri, U.S.",
    died: "April 21, 1910 (aged 74) Redding, Connecticut, U.S.",
    occupation: "Writer, humorist, entrepreneur, publisher, lecturer",
    id: 3,
    link: "https://en.wikipedia.org/wiki/Mark_Twain"
  },
  {
    name: 'Leo_Tolstoy',
    book: [{
      name: 'Anna Karenina',
      ISBN: 1984306383
    }, {
      name: 'resurrection',
      ISBN: 0199555761
    }],
    description: "Born to an aristocratic Russian family in 1828,[3] he is best known for the novels War and Peace (1869) and Anna Karenina (1877),[4] often cited as pinnacles of realist fiction.[3] He first achieved literary acclaim in his twenties with his semi-autobiographical trilogy, Childhood, Boyhood, and Youth (1852–1856), and Sevastopol Sketches (1855), based upon his experiences in the Crimean War. Tolstoy's fiction includes dozens of short stories and several novellas such as The Death of Ivan Ilyich (1886), Family Happiness (1859), and Hadji Murad (1912). He also wrote plays and numerous philosophical essays.In the 1870s Tolstoy experienced a profound moral crisis, followed by what he regarded as an equally profound spiritual awakening, as outlined in his non-fiction work A Confession (1882). His literal interpretation of the ethical teachings of Jesus, centering on the Sermon on the Mount, caused him to become a fervent Christian anarchist and pacifist.[3] Tolstoy's ideas on nonviolent resistance, expressed in such works as The Kingdom of God Is Within You (1894), were to have a profound impact on such pivotal 20th-century figures as Mohandas Karamchand Gandhi,[5] and Martin Luther King, Jr.[6] Tolstoy also became a dedicated advocate of Georgism, the economic philosophy of Henry George, which he incorporated into his writing, particularly Resurrection (1899)",
    born: "Lev Nikolaevich Tolstoy September 9, 1828 Yasnaya Polyana, Tula Governorate, Russian Empire",
    died: "November 20, 1910 (aged 82) Astapovo, Ryazan Governorate,Russian Empire",
    occupation: "Novelist, short story writer,playwright,essayist",
    id: 4,
    link: "https://en.wikipedia.org/wiki/Leo_Tolstoy"
  },
  {
    name: 'Jane_Austen',
    book: [{
      name: "emma",
      ISBN: 1514696886
    }, {
      name: "pride and prejudice",
      ISBN: 9781503290563
    }],
    description: "Jane Austen 16 December 1775 – 18 July 1817) was an English novelist known primarily for her six major novels, which interpret, critique and comment upon the British landed gentry at the end of the 18th century. Austen's plots often explore the dependence of women on marriage in the pursuit of favourable social standing and economic security. Her works critique the novels of sensibility of the second half of the 18th century and are part of the transition to 19th-century literary realism.[2][b] Her use of biting irony, along with her realism, humour, and social commentary, have long earned her acclaim among critics, scholars, and popular audiences alike.[4]With the publications of Sense and Sensibility (1811), Pride and Prejudice (1813), Mansfield Park (1814) and Emma (1816), she achieved success as a published writer. She wrote two additional novels, Northanger Abbey and Persuasion, both published posthumously in 1818, and began another, eventually titled Sanditon, but died before its completion. She also left behind three volumes of juvenile writings in manuscript, a short epistolary novel Lady Susan, and another unfinished novel, The Watsons. Her six full-length novels have rarely been out of print, although they were published anonymously and brought her moderate success and little fame during her lifetime.A significant transition in her posthumous reputation occurred in 1833, when her novels were republished in Richard Bentley's Standard Novels series, illustrated by Ferdinand Pickering, and sold as a set.[5] They gradually gained wider acclaim and popular readership. In 1869, fifty-two years after her death, her nephew's publication of A Memoir of Jane Austen introduced a compelling version of her writing career and supposedly uneventful life to an eager audience.Austen has inspired a large number of critical essays and literary anthologies. Her novels have inspired many films, from 1940's Pride and Prejudice to more recent productions like Sense and Sensibility (1995), Emma (1996), Mansfield Park (1999), Pride & Prejudice (2005), and Love & Friendship (2016).",
    born: "16 December 1775 Steventon Rectory, Hampshire,England",
    died: "18 July 1817 (aged 41) Winchester, Hampshire, England",
    occupation: "writer",
    id: 5,
    link: "https://en.wikipedia.org/wiki/Jane_Austen"
  }
];
const booksData = [{
    name: "Lolita",
    authorId: 1,
    description: 'Lolita, a novel by Vladimir Nabokov was published in 1955 and is one of the finest novels of the 20th century. The story is a bit controversial which revolves around Humbert Humbert. He is in love with a 12 year old girl named Dolores Haze and calls her privately Lolita. Humbert Humbert is a middle-aged professor going through mid-life crisis, who falls for the daughter of his landlady. He has become very possessive about her and can do anything to have her. So over possessive he is that he can commit any crime in order to get her. Is this love or a level of insanity? Is he a frustrated soul or a monster? This book is a stunning work of art of today’s times. Lolita will keep you glued to your chairs till the very end. It involves elements of comedy and is very expressive in its approach.',
    ISBN: 9780679723165,
    paperback: "317",
    publisher: "Vintage (March 13, 1989)",
    review: "4.5 star",
    link: "https://en.wikipedia.org/wiki/Lolita"
  },
  {
    name: "Pale Fire",
    authorId: 1,
    description: "The American poet John Shade is dead; murdered. His last poem, Pale Fire, is put into a book, together with a preface, a lengthy commentary and notes by Shade's editor, Charles Kinbote. Known on campus as the 'Great Beaver', Kinbote is haughty, inquisitive, intolerant, but is he also mad, bad - and even dangerous? As his wildly eccentric annotations slide into the personal and the fantastical, Kinbote reveals perhaps more than he should.",
    ISBN: 9780141185262,
    paperback: "224",
    publisher: "Vintage; Reissue edition (April 23, 1989)",
    review: "4.65 star",
    link: "https://en.wikipedia.org/wiki/Pale_Fire"
  },
  {
    name: 'Laughter in the dark',
    authorId: 1,
    description: "Albinus - rich, married middle-aged and respectable - is an art critic and aspiring filmmaker who lusts after the coquettish young cinema usherette Margot. Gradually he seduces her and convinces himself he is irresistible to her, but Margot has other plans. She wants to be a film star, and when Albinus introduces her to the American movie producer Axel Rex, she sees her chance - and plotting, duplicity and tragedy ensue.",
    ISBN: 9780811216746,
    paperback: "304",
    publisher: "Vintage; 1st Vintage international ed edition (December 17, 1989)",
    review: "4.45 star",
    link: "https://en.wikipedia.org/wiki/Laughter_in_the_Dark_(novel)"
  },
  {
    name: 'Ulysees',
    authorId: 2,
    description: "Ulysses takes place in a single day, 16 June 1904, also known as Bloomsday, it sets the characters and incidents of the Odyssey of Homer in modern Dublin and represents Odysseus (Ulysses), Penelope and Telemachus in the characters of Leopold Bloom, his wife Molly Bloom and Stephen Dedalus, and contrasts them with their lofty models. The book explores various areas of Dublin life, dwelling on its squalor and monotony. Nevertheless, the book is also an affectionately detailed study of the city. In Ulysses, Joyce employs stream of consciousness, parody, jokes, and virtually every other literary technique to present his characters. Many consider it the best novel of the twentieth century. It is powerfully written, a book for the ages.",
    ISBN: 9781604598636,
    paperback: "682",
    publisher: "Digireads.com (September 18, 2016)",
    review: "3.5 star",
    link: "https://en.wikipedia.org/wiki/Ulysses_(novel)"
  },
  {
    name: 'Dubliners',
    authorId: 2,
    description: "The debut of Ireland's greatest author and one of the most influential voices in modern literature Dubliners is a collection of fifteen short stories by James Joyce. By portraying successively incidents in the childhood, adolescence, maturity, and public life of Dubliners, Joyce provides a picture of the suffocating world from which he fled.The collection includes two of Joyce's most famous short stories, Araby and The Dead.",
    ISBN: 6069831195,
    paperback: "108",
    publisher: "Independently published (June 15, 2018)",
    review: "4.1 star",
    link: "https://en.wikipedia.org/wiki/Dubliners"
  },
  {
    name: 'The Dead',
    authorId: 2,
    description: "THE DEAD is the final short story in the 1914 collection Dubliners by James Joyce. It is the longest story in the collection and is often considered the best of Joyce's shorter works.The story centres on Gabriel Conroy on the night of the Morkan sisters' annual dance and dinner in the first week of January 1904, perhaps the Feast of the Epiphany (January 6). Typical of the stories in Dubliners, 'The Dead' develops toward a moment of painful self-awareness; Joyce described this as an epiphany. The narrative generally concentrates on Gabriel's insecurities, his social awkwardness, and the defensive way he copes with his discomfort.",
    ISBN: 1629101648,
    paperback: "82",
    publisher: "Coyote Canyon Press (June 18, 2009)",
    review: "3.9 star",
    link: "https://en.wikipedia.org/wiki/The_Dead_(short_story)"
  },
  {
    name: 'Araby',
    authorId: 2,
    description: "Araby is a short story by the Irish writer James Joyce, published in the 1914 collection Dubliners.Through first-person narration, the reader is immersed at the start of the story in the drab life that people live on North Richmond Street, which seems to be illuminated only by the verve and imagination of the children who, despite the growing darkness that comes during the winter months, insist on playing until [their] bodies glowed. Even though the conditions of this neighbourhood leave much to be desired, the children’s play is infused with their almost magical way of perceiving the world, which the narrator dutifully conveys to the reader.A teenage boy falls in love with his friend's sister and must finagle a trip to a bazaar at nearby Araby to win her heart. But will it be that easy?",
    ISBN: 1502734524,
    paperback: "312",
    publisher: "Vintage (March 13, 1989)",
    review: "3.9 star",
    link: "https://en.wikipedia.org/wiki/Araby_(short_story)"
  },
  {
    name: 'The Adventures of Huckleberry Finn',
    uthorId: 3,
    description: "Mark Twain's witty, satirical tale of childhood rebellion against hypocritical adult authority, the Penguin Classics edition of The Adventures of Huckleberry Finn is edited with a critical introduction by Peter Coveney. Mark Twain's story of a boy's journey down the Mississippi on a raft conveyed the voice and experience of the American frontier as no other work had done before. When Huck escapes from his drunken, abusive 'Pap' and the 'sivilizing' Widow Douglas with runaway slave Jim, he embarks on a series of adventures that draw him to feuding families and the trickery of the unscrupulous 'Duke' and 'Dauphin'.",
    ISBN: 9780141439648,
    paperback: "224",
    publisher: "Dover Publications; 1 edition (May 26, 1994)",
    review: "4.3 star",
    link: "https://en.wikipedia.org/wiki/Adventures_of_Huckleberry_Finn"
  },
  {
    name: 'The adventures of Tom sawyer',
    uthorId: 3,
    description: "The Adventures of Tom Sawyer by Mark Twain is an 1876 novel about a young boy growing up along the Mississippi River. The story is set in the fictional town of St. Petersburg, inspired by Hannibal, Missouri, where Twain lived. Tom Sawyer lives with his Aunt Polly and his half-brother Sid. Tom dirties his clothes in a fight and is made to whitewash the fence the next day as punishment. He cleverly persuades his friends to trade him small treasures for the privilege of doing his work. He then trades the treasures for Sunday School tickets which one normally receives for memorizing verses, redeeming them for a Bible, much to the surprise and bewilderment of the superintendent who thought it was simply preposterous that this boy had warehoused two thousand sheaves of Scriptural wisdom on his premises—a dozen would strain his capacity, without a doubt.",
    ISBN: 1503215679,
    paperback: "168",
    publisher: "CreateSpace Independent Publishing Platform (October 25, 2018)",
    review: "4.5 star",
    link: "https://en.wikipedia.org/wiki/The_Adventures_of_Tom_Sawyer"
  },
  {
    name: 'Anna Karenina',
    authorId: 4,
    description: "Anna Karenina ( is a novel by the Russian writer Leo Tolstoy, published in serial installments from 1873 to 1877 in the periodical The Russian Messenger. Tolstoy clashed with editor Mikhail Katkov over political issues that arose in the final installment (Tolstoy's negative views of Russian volunteers going to fight in Serbia); therefore, the novel's first complete appearance was in book form in 1878. Widely regarded as a pinnacle in realist fiction, Tolstoy considered Anna Karenina his first true novel, after he came to consider War and Peace to be more than a novel. Fyodor Dostoyevsky declared it flawless as a work of art.",
    ISBN: 1984306383,
    paperback: "819",
    publisher: "Oxford University Press; Reissue edition (November 28, 2017)",
    review: "4.5 star",
    link: "https://en.wikipedia.org/wiki/Anna_Karenina"
  },
  {
    name: 'resurrection',
    authorId: 4,
    description: "Resurrection, the last of Tolstoy's major novels, tells the story of a nobleman's attempt to redeem himself for the suffering his youthful philandering caused a peasant girl. Tolstoy's vision of redemption achieved through loving forgiveness, and his condemnation of violence dominate the novel. An intimate, psychological tale of guilt, anger, and forgiveness, Resurrection is at the same time a panoramic description of social life in Russia at the end of the nineteenth century, reflecting Tolstoy's outrage at the social injustices of the world in which he lived.",
    ISBN: 9780199555765,
    paperback: "528",
    publisher: "Oxford University Press; Reissue edition (August 29, 2009)",
    review: "4 star",
    link: "https://en.wikipedia.org/wiki/Resurrection_(novel)"
  },
  {
    name: 'emma',
    authorId: 5,
    description: "Emma, by Jane Austen, is a novel about youthful hubris and the perils of misconstrued romance. The novel was first published in December 1815. As in her other novels, Austen explores the concerns and difficulties of genteel women living in Georgian-Regency England; she also creates a lively comedy of manners among her characters. Before she began the novel, Austen wrote, I am going to take a heroine whom no one but myself will much like. In the first sentence she introduces the title character as Emma Woodhouse, handsome, clever, and rich. Emma is spoiled, headstrong, and self-satisfied; she greatly overestimates her own matchmaking abilities; she is blind to the dangers of meddling in other people's lives; and her imagination and perceptions often lead her astray.",
    ISBN: 1514696886,
    paperback: "264",
    publisher: "CreateSpace Independent Publishing Platform (November 1, 2018)",
    review: "4.3 star",
    link: "https://en.wikipedia.org/wiki/Emma_(novel)"
  },
  {
    name: 'pride and prejudice',
    authorId: 5,
    description: "Pride and Prejudice is a novel of manners by Jane Austen, first published in 1813. The story follows the main character, Elizabeth Bennet, as she deals with issues of manners, upbringing, morality, education, and marriage in the society of the landed gentry of the British Regency. Elizabeth is the second of five daughters of a country gentleman living near the fictional town of Meryton in Hertfordshire, near London. Page 2 of a letter from Jane Austen to her sister Cassandra (11 June 1799) in which she first mentions Pride and Prejudice, using its working title First Impressions. Set in England in the early 19th century, Pride and Prejudice tells the story of Mr and Mrs Bennet's five unmarried daughters after the rich and eligible Mr Bingley and his status-conscious friend, Mr Darcy, have moved into their neighbourhood. ",
    ISBN: 9781503290563,
    paperback: "336",
    publisher: "Puffin Books (October 16, 2018)",
    review: "5 star",
    link: "https://en.wikipedia.org/wiki/Pride_and_Prejudice"
  }
]

function getAuthor(authorsData, key) {
  return authorsData.filter(authorData => authorData['id'] === key);
}

function getBook(booksData, key) {
  return booksData.filter(bookData => bookData['ISBN'] === key)[0];
}

module.exports = {
  authorsData,
  booksData,
  getAuthor,
  getBook
}
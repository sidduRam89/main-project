const bcrypt = require('bcrypt');

function generateHashPassword(password) {
  const saltRound = 10;
  const salt = bcrypt.genSaltSync(saltRound);
  const hashedProctected = bcrypt.hashSync(password, salt);
  return hashedProctected;
}
function comparePassword(password, hash) {
  const result = bcrypt.compareSync(password, hash);
  return result;
}
module.exports = {
  generateHashPassword,
  comparePassword,
};

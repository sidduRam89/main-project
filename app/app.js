/* eslint-disable import/no-unresolved */
const express = require('express');
const { bookRouter } = require('../src/book/bookRoute.js');
const { authorRouter } = require('../src/author/authorRoute.js');
const { userRouter } = require('../src/user/userRouter.js');
const { logger, loggerPort, errorLogger } = require('../middlewares/logger.js');
const { nocache } = require('../middlewares/nocache.js');
const { fileNotFound } = require('../middlewares/fileNotFound.js');

const port = 5000;
const app = express();
app.set('view engine', 'ejs');
app.use('/public', express.static('public'));
// to handle logs
app.use(logger);
app.use(nocache);
app.use(userRouter);
app.use(bookRouter);
app.use(authorRouter);
// to handle errors
app.use(fileNotFound);
app.use(errorLogger);
app.listen(port, loggerPort(port));

module.exports = {
  app,
};

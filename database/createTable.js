/* eslint-disable no-console */
const mysql = require('promise-mysql');

async function dbConnection() {
  const db = await mysql.createConnection({
    host: 'localhost',
    user: 'siddu',
    password: 'iamsiddu',
    database: process.env.testDB || 'main_project',
  });
  return db;
}
async function createAuthorTable() {
  try {
    const db = await dbConnection();
    const query = 'create table if not exists authorData(name varchar(255) not null,description varchar(2200) not null,born varchar(255)  not null, died varchar(255) not null,occupation varchar(255) not null,id int primary key not null,link varchar(255) not null)';
    await db.query(query);
    console.log('author table created');
    db.end();
  } catch (err) {
    console.error(err);
  }
}
async function createBookTable() {
  try {
    const db = await dbConnection();
    const query = 'create table if not exists bookData(name varchar(255) not null,authorId int not null ,description varchar(1000) not null,ISBN bigint(20) not null primary key,paperback varchar(255)not null,publisher varchar(255) not null,review varchar(255) not null,link varchar(255) not null,foreign key(authorId) references authorData(id))';
    await db.query(query);
    console.log('book table created');
    db.end();
  } catch (err) {
    console.error(err);
  }
}
createAuthorTable();
createBookTable();

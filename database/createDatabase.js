/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
const mysql = require('promise-mysql');

async function createDb() {
  const db = await mysql.createConnection({
    host: 'localhost',
    user: 'siddu',
    password: 'iamsiddu',
  });
  await db.query('create database if not exists main_project');
  console.log('database created');
  db.end();
}

createDb();

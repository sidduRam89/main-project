/* eslint-disable no-console */
const mysql = require('promise-mysql');

async function dropTables() {
  try {
    const db = await mysql.createConnection({
      host: 'localhost',
      user: 'siddu',
      password: 'iamsiddu',
      database: 'main_project',
    });
    await db.query('drop database if exists main_project');
    console.log('main_project database dropped');
    db.end();
  } catch (err) {
    console.error(err);
  }
}
dropTables();

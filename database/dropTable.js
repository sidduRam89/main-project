/* eslint-disable no-console */
const mysql = require('promise-mysql');

async function dropTables() {
  try {
    const db = await mysql.createConnection({
      host: 'localhost',
      user: 'siddu',
      password: 'iamsiddu',
      database: 'main_project',
    });
    await db.query('drop table if exists bookData');
    console.log('bookData table dropped');
    await db.query('drop table if exists authorData');
    console.log('authorData table dropped');
    db.end();
  } catch (err) {
    console.error(err);
  }
}
dropTables();

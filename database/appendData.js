/* eslint-disable no-console */
/* eslint-disable no-await-in-loop */
const mysql = require('promise-mysql');
const { authorsData } = require('../app/data.js');
const { booksData } = require('../app/data.js');

async function dbConnection() {
  const db = mysql.createConnection({
    host: 'localhost',
    user: 'siddu',
    password: 'iamsiddu',
    database: 'main_project',
  });
  return db;
}
async function appendAuthorsData() {
  try {
    const db = await dbConnection();
    let values;
    const sqlQuery = 'insert into authorData(name,description,born,died,occupation,id,link) values ?';
    authorsData.forEach(async (authorData) => {
      values = (Object.values(authorData).filter(value => typeof value !== 'object'));
      values = [[values]];
      await db.query(sqlQuery, values);
    });
    console.log('authors data has been appended into authorData table');
    db.end();
  } catch (err) {
    console.error(err);
  }
}
async function appendBooksData() {
  try {
    const db = await dbConnection();
    let values;
    const sqlQuery = `insert into bookData(${Object.keys(booksData[0]).join(',')}) values ?`;
    booksData.forEach(async (bookData) => {
      values = (Object.values(bookData));
      values = [[values]];
      await db.query(sqlQuery, values);
    });
    console.log('books data has been appended into bookData table');
    db.end();
  } catch (err) {
    console.error(err);
  }
}
appendAuthorsData();
appendBooksData();

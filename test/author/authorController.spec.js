/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
const { expect } = require('chai');
const { getAuthorsData, getAuthorById } = require('../../src/author/authorController.js');

describe('test database', () => {
  it('get authors data', async () => {
    const authorsData = await getAuthorsData();
    const expected = [{
      name: 'hari',
      id: 1,
    }, {
      name: 'pari',
      id: 2,
    }];
    expect(authorsData).to.deep.equal(expected);
  });

  it('get authors by id', async () => {
    const authorKey = 1;
    const authorsData = await getAuthorById(authorKey);
    const expected = {
      name: 'hari',
      description: 'aaaaaa',
      born: '12 dec',
      died: '13 dec',
      occupation: 'writer',
      id: 1,
      link: '#',
      book: [['pale', 10], ['fire', 11]],
    };
    expect(authorsData).to.deep.equal(expected);
  });
  it('out of index author', async () => {
    const authorKey = 20;
    const authorsData = await getAuthorById(authorKey);
    expect(authorsData).to.be.empty;
  });
});

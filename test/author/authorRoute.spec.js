/* eslint-disable no-undef */
const request = require('supertest');
const { app } = require('../../app/app.js');

describe('book route test', () => {
  it('Unauthorized author page', (done) => {
    request(app)
      .get('/author')
      .expect(401)
      .expect(/Unauthorized User/, done);
  });
});

/* eslint-disable no-undef */
const request = require('supertest');
const { app } = require('../../app/app.js');

describe('book route test', () => {
  it('Unauthorized book page', (done) => {
    request(app)
      .get('/book')
      .expect(401)
      .expect(/Unauthorized User/, done);
  });
});

/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
const { expect } = require('chai');
const { getBooksData, getBookByIsbn } = require('../../src/book/bookContoller.js');

describe('book data', () => {
  it('get books data ', async () => {
    const booksData = await getBooksData();
    const expected = [
      {
        name: 'pale',
        authorId: 1,
        authorName: 'hari',
        description: 'aaaaa',
        ISBN: 10,
        paperback: '100 pages',
        publisher: 'ssss',
        review: 'good',
        link: '#',
      },
      {
        name: 'fire',
        authorId: 1,
        authorName: 'hari',
        description: 'aaaaa',
        ISBN: 11,
        paperback: '110 pages',
        publisher: 'ssss',
        review: 'good',
        link: '#',
      },
      {
        name: 'good reads',
        authorId: 2,
        authorName: 'pari',
        description: 'bbbb',
        ISBN: 12,
        paperback: '110 pages',
        publisher: 'ssss',
        review: 'bad',
        link: '#',
      },
    ];
    expect(booksData).to.deep.equal(expected);
  });

  it('get book by isbn', async () => {
    const bookISBN = 12;
    const booksData = await getBookByIsbn(bookISBN);
    const expected = [{
      name: 'good reads',
      authorName: 'pari',
      authorId: 2,
      description: 'bbbb',
      ISBN: 12,
      paperback: '110 pages',
      publisher: 'ssss',
      review: 'bad',
      link: '#',
    }];
    expect(booksData).to.deep.equal(expected);
  });

  it('out of index book', async () => {
    const bookISBN = 20;
    const booksData = await getBookByIsbn(bookISBN);
    expect(booksData).to.be.empty;
  });
});
